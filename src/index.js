import React from 'react';
import './index.css'

// components
// import FirstApp from './FirstApp';
import CounterApp from './CounterApp';


import ReactDOM from 'react-dom';


const divRoot = document.querySelector('#root');

ReactDOM.render(<CounterApp counter={1234} />, divRoot);
